import React, {useState} from 'react';
import { Form, TextArea, Input } from 'semantic-ui-react';
import axios from 'axios';

export default function ContactForm () {

  const [inputs, setInputs] = useState({
    email: "",
    message: ""
  })

  const handleOnChange = event => {
    event.persist();
    setInputs(prev => ({
      ...prev,
      [event.target.id]: event.target.value
    }));
  }
  
  const [serverState, setServerState] = useState({
    submitting: false,
    status: null
  });

  const handleServerResponse = (ok, msg) => {
    setServerState({
      submitting: false,
      status: { ok, msg }
    });
    if (ok) {
      setInputs({
        email: "",
        message: ""
      })
    }
  }

  const handleOnSubmit = event => {
    event.preventDefault();
    axios({
      method: "POST",
      url: "https://formspree.io/mgekqpwp",
      data: inputs
    })
    .then(r => {
      handleServerResponse(true, "Thanks");
    })
    .catch(r => {
      handleServerResponse(false, r.response.data.error);
    })
  }

    return (
      <Form fluid align='center' onSubmit={ handleOnSubmit } >
        <h1>Contact Us</h1>
        <Form.Group widths='equal'>
          <Form.Field
              control={ Input }
              label='Name'
              id='name'
              type='text'
              name='name'
              onChange = { handleOnChange }
              required
              value = {inputs.name}
              />
              <Form.Field
              control={ Input } 
              label='Email'
              id='email'
              type='email'
              name='email'
              onChange = { handleOnChange }
              value = { inputs.email }
              required
            />                            
        </Form.Group>  
        <Form.Field 
        control= {TextArea}
        label= 'Message'
        id='message' 
        fluid type= 'text' 
        name= 'message'
        onChange = { handleOnChange }
        value = { inputs.message } 
        required
        />
      <button type="submit" disabled={serverState.submitting}>
        Submit
      </button>
      { serverState.status && (
    <p className={!serverState.status.ok ? "errorMsg" : ""}>
      {serverState.status.msg}
    </p>
  )}
      </Form>
  )}